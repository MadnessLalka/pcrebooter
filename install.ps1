function isAdministratorPriviliges {
    $currentUser = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
    $isAdmin = $currentUser.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)

    if ($isAdmin) {
        makeAndCopyFolderHierarchy
    }
    else {
        Write-Host "You need to start this script with administrator priviliges" -ForegroundColor Red
        Pause
    }
}

function CreateTasklScheduler {
    $action = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "-WindowStyle hidden C:\Script\PCRebooter\main.ps1"
    $trigger = New-ScheduledTaskTrigger -Daily -At "16:00"
    $principal = New-ScheduledTaskPrincipal -GroupId "S-1-5-11" -RunLevel Limited
    Register-ScheduledTask -TaskName "PcReboot" -Action $action -Trigger $trigger -Principal $principal
}

function makeAndCopyFolderHierarchy {
    $makeFolder = New-Item -Path "C:\Script\PCRebooter\" -ItemType Directory

    if ($makeFolder) {
        
        Copy-Item -Path "main.ps1" -Destination "C:\Script\PCRebooter\"
        if ($?) {
            $isTaskSchedul = CreateTasklScheduler
            if ($isTaskSchedul) {
                Write-Host "Task was schedule" -ForegroundColor Green
                Write-Host "Script install successful" -ForegroundColor Green
                Pause
            }
            else {
                Write-Host "Task was not schedule" -ForegroundColor Red
                Pause
            }
        }
        else {
            Write-Host $copyItems
            Write-Host "main.ps1 was not copy" -ForegroundColor Red
            Pause
        }
    }
    else {
        Write-Host "The folder hierarchy was not created" -ForegroundColor Red
        Pause
    }
}

isAdministratorPriviliges