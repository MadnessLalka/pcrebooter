﻿$scriptName = $MyInvocation.MyCommand.Name
Start-Process PowerShell.exe -ArgumentList "-WindowStyle Hidden -File'$scriptName'"

$global:Minutes = 30
$global:Timer = $global:Minutes * 60
$global:isTimerWaiterOn = $false
function TimerBackend {
    if ($global:Timer -eq 0) {
        msg * "Компьютер будет перезагружен через 5 минут"
        $main_form.WindowState = 'Minimized'
        $Time.Stop()
        $Time.Dispose()
        shutdown.exe -r -t 300
    }
    elseif ($global:Timer -eq 300 -and $global:isTimerWaiterOn) {
        $main_form.WindowState = 'Normal'
        $global:isTimerWaiterOn = $false
    }
    else {
        $integerMinutes = ([int][math]::Truncate($global:Timer / 60)).ToString("00")
        $integerSeconds = ([int]($global:Timer % 60)).ToString("00")

        $LabelTimerMinutes.Text = "$($integerMinutes)"
        $LabelTimerSeconds.Text = "$($integerSeconds)"
        $global:Timer--
        # Write-Host $global:Timer
        # Write-Host $integerMinutes 
        # Write-Host [int](($global:Timer % 60))
    }
    
}
function ButtonClickWait {
    $global:Timer += $Minutes * 60
    $main_form.WindowState = 'Minimized'
    $global:isTimerWaiterOn = $true
}

function ButtonClickReboot {
    msg * "Компьютер будет перезагружен через 5 минут"
    $main_form.WindowState = 'Minimized'
    $Time.Stop()
    $Time.Dispose()  
    shutdown.exe -r -t 300

}

Add-Type -AssemblyName System.Windows.Forms
$main_form = New-Object System.Windows.Forms.Form
$main_form.Text = 'PcRebooter'
$main_form.Width = 600
$main_form.Height = 400
$main_form.AutoSize = $true
$main_form.WindowState = 'Normal'
$main_form.StartPosition = "CenterScreen"
$main_form.TopMost = $true
$main_form.ControlBox = $false

$Label = New-Object System.Windows.Forms.Label
$Label.Text = "Компьютер перезагрузится через:"
$Label.Font = 'Microsoft Sans Serif,25'
$Label.Location = New-Object System.Drawing.Point(30, 20)
$Label.AutoSize = $true
$main_form.Controls.Add($Label)

$Time = New-Object System.Windows.Forms.Timer
$Time.Interval = 1000
$Time.Start()
$Time.Add_Tick({ TimerBackend })

$LabelTimerMinutes = New-Object System.Windows.Forms.Label
$LabelTimerMinutes.Text = "00"
$LabelTimerMinutes.Font = 'Microsoft Sans Serif,50'
$LabelTimerMinutes.Location = New-Object System.Drawing.Point(180, 80)
$LabelTimerMinutes.AutoSize = $true
$main_form.Controls.Add($LabelTimerMinutes)

$LabelTextSeparator = New-Object System.Windows.Forms.Label
$LabelTextSeparator.Text = ":"
$LabelTextSeparator.Font = 'Microsoft Sans Serif,50'
$LabelTextSeparator.Location = New-Object System.Drawing.Point(270, 80)
$LabelTextSeparator.AutoSize = $true
$main_form.Controls.Add($LabelTextSeparator)

$LabelTimerSeconds = New-Object System.Windows.Forms.Label
$LabelTimerSeconds.Text = "00"
$LabelTimerSeconds.Font = 'Microsoft Sans Serif,50'
$LabelTimerSeconds.Location = New-Object System.Drawing.Point(300, 80)
$LabelTimerSeconds.AutoSize = $true
$main_form.Controls.Add($LabelTimerSeconds)

$ButtonReboot = New-Object System.Windows.Forms.Button
$ButtonReboot.Text = "Перезагрузка"
$ButtonReboot.Font = 'Microsoft Sans Serif,20'
$ButtonReboot.Location = New-Object System.Drawing.Point(45, 180)
$ButtonReboot.Size = New-Object System.Drawing.Size(120, 100)
$ButtonReboot.AutoSize = $true
$main_form.Controls.Add($ButtonReboot)

$ButtonWait = New-Object System.Windows.Forms.Button
$ButtonWait.Text = "Отложить на " + $Minutes + " минут"
$ButtonWait.Font = 'Microsoft Sans Serif,18'
$ButtonWait.Location = New-Object System.Drawing.Point(280, 180)
$ButtonWait.Size = New-Object System.Drawing.Size(120, 100)
$ButtonWait.AutoSize = $true
$main_form.Controls.Add($ButtonWait)

$ButtonWait.Add_Click({ ButtonClickWait })
$ButtonReboot.Add_Click({ ButtonClickReboot })
$main_form.ShowDialog()